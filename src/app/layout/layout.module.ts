import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";

import { CustomBreakPointsProvider } from "app/global-data/layout/custom-breakpoints";
import { SharedModule } from "app/shared/shared.module";
import { LayoutRoutingModule } from "./layout-routing.module";
import { LayoutComponent } from "./layout.component";

@NgModule({
  declarations: [LayoutComponent],
  imports: [
    FlexLayoutModule.withConfig({ disableDefaultBps: true }),
    LayoutRoutingModule,
    SharedModule,
  ],
  providers: [CustomBreakPointsProvider],
})
export class LayoutModule {}

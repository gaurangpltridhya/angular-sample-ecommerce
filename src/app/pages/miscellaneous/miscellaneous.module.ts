import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ComingSoonComponent } from "./coming-soon/coming-soon.component";
import { ErrorComponent } from "./error/error.component";
import { MaintenanceComponent } from "./maintenance/maintenance.component";

import { MiscellaneousRoutingModule } from "./miscellaneous-routing.module";
import { NotAuthorizedComponent } from "./not-authorized/not-authorized.component";

@NgModule({
  declarations: [
    ComingSoonComponent,
    NotAuthorizedComponent,
    MaintenanceComponent,
    ErrorComponent,
  ],
  imports: [CommonModule, MiscellaneousRoutingModule],
})
export class MiscellaneousModule {}

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AuthForgotPasswordComponent } from "./auth-forgot-password/auth-forgot-password.component";
import { AuthLoginComponent } from "./auth-login/auth-login.component";
import { AuthRegisterComponent } from "./auth-register/auth-register.component";
import { AuthResetPasswordComponent } from "./auth-reset-password/auth-reset-password.component";

import { AuthenticationRoutingModule } from "./authentication-routing.module";

@NgModule({
  declarations: [
    AuthLoginComponent,
    AuthRegisterComponent,
    AuthForgotPasswordComponent,
    AuthResetPasswordComponent,
  ],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AuthenticationModule {}

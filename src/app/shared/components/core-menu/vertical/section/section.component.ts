import { Component, Input } from "@angular/core";
import { CoreMenuItem } from "app/models/core-menu";

@Component({
  selector: "[core-menu-vertical-section]",
  templateUrl: "./section.component.html",
})
export class CoreMenuVerticalSectionComponent {
  @Input()
  item: CoreMenuItem;
}

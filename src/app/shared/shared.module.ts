import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import {
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
} from "ngx-perfect-scrollbar";
import { SideMenuComponent } from "./components/side-menu/side-menu.component";

import { RouterModule } from "@angular/router";
import { ContentComponent } from "./components/content/content.component";
import { BreadcrumbComponent } from "./components/content-header/breadcrumb/breadcrumb.component";
import { ContentHeaderComponent } from "./components/content-header/content-header.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { MenuComponent } from "./components/menu/menu.component";
import { RippleEffectDirective } from "./directives/core-ripple-effect.directive";
import { FeatherIconDirective } from "./directives/core-feather-icons";
import { InitialsPipe } from "./pipes/initials.pipe";
import { FilterPipe } from "./pipes/filter.pipe";
import { StripHtmlPipe } from "./pipes/stripHtml.pipe";
import { SafePipe } from "./pipes/safe.pipe";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { CoreMenuComponent } from "./components/core-menu/core-menu.component";
import { CoreMenuVerticalSectionComponent } from "./components/core-menu/vertical/section/section.component";
import { CoreMenuVerticalItemComponent } from "./components/core-menu/vertical/item/item.component";
import { CoreMenuVerticalCollapsibleComponent } from "./components/core-menu/vertical/collapsible/collapsible.component";
import { CoreSidebarComponent } from "./components/core-sidebar/core-sidebar.component";
import { CoreThemeCustomizerComponent } from "./components/theme-customizer/theme-customizer.component";
import { LazyLoadImageModule } from "ng-lazyload-image";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelPropagation: false,
};
@NgModule({
  declarations: [
    SideMenuComponent,
    MenuComponent,
    ContentComponent,
    BreadcrumbComponent,
    ContentHeaderComponent,
    NavbarComponent,
    RippleEffectDirective,
    FeatherIconDirective,
    InitialsPipe,
    FilterPipe,
    StripHtmlPipe,
    SafePipe,
    CoreMenuComponent,
    CoreMenuVerticalSectionComponent,
    CoreMenuVerticalItemComponent,
    CoreMenuVerticalCollapsibleComponent,
    CoreSidebarComponent,
    CoreThemeCustomizerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    RouterModule,
    NgbModule,
    FlexLayoutModule,
    TranslateModule.forChild(),
    LazyLoadImageModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    LazyLoadImageModule,
    SideMenuComponent,
    MenuComponent,
    ContentComponent,
    BreadcrumbComponent,
    ContentHeaderComponent,
    NavbarComponent,
    RippleEffectDirective,
    FeatherIconDirective,
    InitialsPipe,
    FilterPipe,
    StripHtmlPipe,
    SafePipe,
    CoreMenuComponent,
    CoreSidebarComponent,
    CoreThemeCustomizerComponent,
  ],
})
export class SharedModule {}
